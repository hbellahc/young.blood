package com.hbellahc.paris.jug.youngblood.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {

    @CsvBindByPosition(position = 0)
    private String institution;

    @CsvBindByPosition(position = 3)
    private String title;

    @CsvBindByPosition(position = 4)
    private String instructors;

    private List<String> instructorList;

    @CsvBindByPosition(position = 6)
    private int year;


    @CsvBindByPosition(position = 8)
    private int participants;


    @CsvBindByPosition(position = 10)
    private int certified;


}

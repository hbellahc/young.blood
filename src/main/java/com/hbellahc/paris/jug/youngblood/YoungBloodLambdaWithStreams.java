package com.hbellahc.paris.jug.youngblood;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoungBloodLambdaWithStreams {

    public static void main(String[] args) {
        SpringApplication.run(YoungBloodLambdaWithStreams.class, args);
    }


}



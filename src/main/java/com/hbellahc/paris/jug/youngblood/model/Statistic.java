package com.hbellahc.paris.jug.youngblood.model;

public record Statistic(int year, int count, int participants, int certified) {
}




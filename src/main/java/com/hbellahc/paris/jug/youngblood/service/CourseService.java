package com.hbellahc.paris.jug.youngblood.service;

import com.hbellahc.paris.jug.youngblood.model.Course;
import com.hbellahc.paris.jug.youngblood.model.InstructorWithCourse;
import com.hbellahc.paris.jug.youngblood.model.Statistic;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class CourseService {


    List<Course> courses;


    @PostConstruct
    public void init() throws URISyntaxException, IOException {
        Path path = Paths.get(ClassLoader.getSystemResource("appendix.csv").toURI());
        CsvToBean<Course> cb = null;
        cb = new CsvToBeanBuilder<Course>(Files.newBufferedReader(path))
                .withType(Course.class)
                .withSkipLines(1)
                .build();
        courses = cb.parse();
    }


    /**
     * O. Get all courses
     *
     * @return list of all courses
     */
    public List<Course> getCourses() {
        return courses;
    }

    /**
     * 1.a Get a list of all titles (map)
     *
     * @return list of all course titles
     */
    public List<String> getTitles() {

        var result = courses
                .stream()
                .map(Course::getTitle)
                .toList();


        return result;
    }

    /**
     * 1.b Update each course with list of instructors (map)
     * // from "instructors": "Eric Klopfer, Philp Tan, Sara Verrilli"
     * // to   "instructorList" : ["Eric Klopfer, Philp Tan, Sara Verrilli]
     *
     * @return return a list of all courses updated
     */

    public List<Course> updateCourses() {

        var result = courses
                .stream()
                .map(
                        c -> {
                            c.setInstructorList(splitAsList(c.getInstructors()));
                            return c;
                        }
                ).toList();


        return result;


    }


    /**
     * 1.c Get a set of all instructors (flatmap)
     *
     * @return set of all instructors
     */
    public Set<String> getInstructors() {

        var result = updateCourses()
                .stream()
                .flatMap(c -> c.getInstructorList().stream())
                .collect(Collectors.toSet());


        return result;


    }

    /**
     * 2.a Sort courses by number of participants (sorted)
     *
     * @return courses sorted by number of participants
     */

    public List<Course> getSortedByParticipants() {

        Comparator<Course> byParticipantComparator = Comparator.comparingInt(Course::getParticipants);


        var result = courses
                .stream()
                .sorted(byParticipantComparator.reversed())
                .toList();


        return result;


    }

    /**
     * 2.b Get a list of all courses with more than 1000 certified (filter)
     *
     * @return filter courses with at least 1000 certified
     */
    public List<Course> filterByAtLeast1000Certified() {

        var result = courses
                .stream()
                .filter(c -> c.getCertified() > 1000)
                .toList();

        return result;

    }


    /**
     * 3.a Getting for each instructor its courses separated by pipe "|" (flatMap, groupingBy and join)
     * instructor : [course1, course2] etc
     *
     * @return map of instructors with their courses
     */
    public Map<String, String> coursesByInstructor() {

        var result = updateCourses()
                .stream()
                .flatMap(c -> c.getInstructorList().stream().map(i -> new InstructorWithCourse(i, c.getTitle())))
                .collect(Collectors.groupingBy(
                        InstructorWithCourse::name,
                        Collectors.mapping(InstructorWithCourse::courses, Collectors.joining(" | "))

                ));
        return result;

    }


    /**
     * 3.b. Statistics by year (map, groupingBy and reduce)
     *
     * @return list of statistic by year
     */
    public List<Statistic> statistics() {

        var result = courses
                .stream()
                .map(CourseService::courseToStatistic)
                .collect(Collectors.groupingBy(
                        Statistic::year,
                        Collectors.mapping(s -> s, Collectors.reducing(this::sumStatistics))
                ))
                .values()
                .stream()
                .flatMap(Optional::stream)
                .toList()
                ;
        return result;

    }


    private Statistic sumStatistics(Statistic s1, Statistic s2) {
        return new Statistic(
                s1.year(),
                s1.count() + s2.count(),
                s1.participants() + s2.participants(),
                s1.certified() + s2.certified());
    }

    private static Statistic courseToStatistic(Course c) {
        return new Statistic(c.getYear(), 1, c.getParticipants(), c.getCertified());
    }

    private List<String> splitAsList(String instructors) {

        return Pattern.compile(",")
                .splitAsStream(instructors)
                .map(StringUtils::trim)
                .toList();


    }


}

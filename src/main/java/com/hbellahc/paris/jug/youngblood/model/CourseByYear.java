package com.hbellahc.paris.jug.youngblood.model;

public record CourseByYear(Integer year, Integer count) {
}


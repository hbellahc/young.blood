package com.hbellahc.paris.jug.youngblood.model;

public record InstructorWithCourse(String name, String courses) {
}

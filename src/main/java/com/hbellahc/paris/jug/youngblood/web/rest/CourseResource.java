package com.hbellahc.paris.jug.youngblood.web.rest;

import com.hbellahc.paris.jug.youngblood.model.Course;
import com.hbellahc.paris.jug.youngblood.model.Statistic;
import com.hbellahc.paris.jug.youngblood.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/api/courses")
public class CourseResource {


    CourseService service;

    @Autowired
    CourseResource(CourseService service) {
        this.service = service;
    }

    @GetMapping("/")
    List<Course> getCourses() {
        return service.getCourses();
    }

    @GetMapping("/titles")
    List<String> getTitles() {
        return service.getTitles();
    }

    @GetMapping("/updated-courses")
    List<Course> updatedCourses() {
        return service.updateCourses();
    }


    @GetMapping("/instructors")
    Set<String> getInstructors() {
        return service.getInstructors();
    }

    @GetMapping("/sorted-by-participants")
    List<Course> sortedByParticipants() {
        return service.getSortedByParticipants();
    }


    @GetMapping("/at-least-1000-certified")
    List<Course> filterByAtLeast1000Certified() {
        return service.filterByAtLeast1000Certified();
    }


    @GetMapping("/courses-by-instructor")
    Map<String, String> coursesByInstructor() {
        return service.coursesByInstructor();
    }


    @GetMapping("/statistics-by-year")
    List<Statistic> statistics() {
        return service.statistics();
    }


}
